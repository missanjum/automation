package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import POM.HomePageObjects;

public class Scenario2 {

	public static void main(String[] args) throws InterruptedException {
	
	// Initialize the Chrome Browser 
		
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		
		  // System Property for Gecko Driver   
		//System.setProperty("webdriver.chrome.driver","D:\\geckodriver.exe");  
		          
		          // Initialize Gecko Driver using Desired Capabilities Class  
		 /*   DesiredCapabilities capabilities = DesiredCapabilities.firefox();  
		    capabilities.setCapability("marionette",true);  
		    WebDriver driver= new FirefoxDriver();  */
		
	// 1---- > Navigate to​ www.tajawal.com  Automation
		
		driver.navigate().to("https:www.tajawal.ae");
		
	// Maximize the Browser Window 
		
	driver.manage().window().maximize();
	
	HomePageObjects Searchpage=new HomePageObjects(driver);
	
	// 2- ---> Entering the value of Origin in Search field
	
	Searchpage.origin.sendKeys("DXB Dubai, United Arab Emirates - Dubai Airport");
		
	Searchpage.Destination.sendKeys("COK Kochi, India - Cochin International Airport");
	//Searchpage.passenger.click();
	// Search for any valid search query (no specific data)
	
	Searchpage.searchicon.click();
	driver.navigate().refresh();
    
	// 3- ---> Sorting the Flights on Search Page in Ascending Order  
	
	driver.navigate().to("https://www.tajawal.ae/en/flights/DXB-COK/2019-03-08/2019-03-21/Economy/3Adult");
	Searchpage.price.click();
	
	}
	// 4----> 
	
	}

