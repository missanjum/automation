package Pages;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Scenario1 {
		
	public static void main(String[] args) throws InterruptedException {
		
		//Initialize the Chrome Browser 
			
			System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
			WebDriver driver=new ChromeDriver();
			
		// Create a new instance of the Chrome driver
			// 1---> Navigate to the Tajawel site 
			
				driver.navigate().to("https://www.tajawal.ae/en");
		// Maximize the Browser Window 
				driver.manage().window().maximize();
		
		//a- Random array of origins (length 5)
				
				
				Random random = new Random();
				String[] originsArray = { "KHI Karachi, Pakistan", "LHE Lahore, Pakistan", "ISB Islamabad, Pakistan", "MUX Multan, Pakistan"};
				WebElement origin = driver.findElement(By.id("flights-search-origin-1"));
				origin.sendKeys(originsArray[random.nextInt(originsArray.length)]);
				
		// b- Random array of destinations (length 5)
				
				String[] destinationsArray = {"SHJ Sharjah, United Arab Emirates", "DXB Dubai, United Arab Emirates", "LHR London, United Kingdom - London Heathrow Airport", "LAS Las Vegas, United States - McCarran International Airport", "IND Indianapolis, United States"};
				
				WebElement destination = driver.findElement(By.id("flights-search-destination-1"));
				destination.sendKeys(destinationsArray[random.nextInt(destinationsArray.length)]);
		// c- Accessing the flights by adding random date and at least 2 adults
				
				driver.findElement(By.id("flights-search-open-pax-btn")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='rbFlightTabContent']/div/div/section/div/div[1]/form/div[3]/div[1]/div[3]/div[2]/div[1]/div[2]/a[2]")).click();
				Thread.sleep(2000);
				driver.findElement(By.id("flights-search-open-pax-btn")).click();
				Thread.sleep(500);
				driver.findElement(By.id("flights-search-cta")).click();
				 
				//Wait for 5 Sec
				Thread.sleep(5000);
				
		// d- Pick Flight details and navigate to details page
				
				driver.navigate().to("https://www.tajawal.ae/en/flight/traveller/cart-58010e88-19d9-45a9-94b4-84cfd8ad0a2b");
				// Close the driver
		        driver.close();
		        driver.quit();

			}


	}

