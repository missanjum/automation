package POM;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePageObjects {
			
public HomePageObjects(WebDriver driver) {
	
	PageFactory.initElements(driver, this);
}

// Identification of Web Locators in POM

@FindBy(id="flights-search-origin-1")
public WebElement origin;
@FindBy(id="flights-search-origin-1")
public WebElement Destination;
@FindBy(id="flights-search-cta")
public WebElement searchicon;

@FindBy(xpath="//*[@id=\"flights-search-cta\"]")
public WebElement searchbtn;

@FindBy(xpath="//*[@id=\"rbFlightTabContent\"]/div/div/section/div/div[1]/form/div[3]/div[1]/div[3]/div[2]/div[1]/div[2]/a[2]")
public WebElement passenger;

//Sorting Parameters and Locators 
@FindBy(xpath="/html/body/div[1]/div[3]/div/ui-view/flight-search-result/div/div[2]/div/div/div[2]/div[1]/div/flights-summary-wrapper/div/div[2]/div[1]/div[5]/span/span") 
public WebElement price;
	


@SuppressWarnings("unused")
public void sendKeys(Keys enter) {
	// TODO Auto-generated method stub
	

	
}

	}


