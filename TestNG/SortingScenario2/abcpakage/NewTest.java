package abcpakage;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

public class NewTest {
	Public static WebDriver driver=null;
  @Test
  public void f() 
  {
	  Random random = new Random();
		String[] originsArray = { "KHI Karachi, Pakistan", "LHE Lahore, Pakistan", "ISB Islamabad, Pakistan", "MUX Multan, Pakistan"};
		WebElement origin = driver.findElement(By.id("flights-search-origin-1"));
		origin.sendKeys(originsArray[random.nextInt(originsArray.length)]);
		
		String[] destinationsArray = {"SHJ Sharjah, United Arab Emirates", "DXB Dubai, United Arab Emirates", "LHR London, United Kingdom - London Heathrow Airport", "LAS Las Vegas, United States - McCarran International Airport", "IND Indianapolis, United States"};
		
		WebElement destination = driver.findElement(By.id("flights-search-destination-1"));
		destination.sendKeys(destinationsArray[random.nextInt(destinationsArray.length)]);
		
		driver.findElement(By.id("flights-search-open-pax-btn")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='rbFlightTabContent']/div/div/section/div/div[1]/form/div[3]/div[1]/div[3]/div[2]/div[1]/div[2]/a[2]")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("flights-search-open-pax-btn")).click();
		Thread.sleep(500);
		driver.findElement(By.id("flights-search-cta")).click();
		 
		//Wait for 5 Sec
		Thread.sleep(5000);
  }
  @BeforeMethod
  public void beforeMethod() 
  {
	  System.setProperty("webdriver.chrome.driver", "C:\\Users\\Admin\\eclipse-workspace\\TUAutomation\\drivers\\chromedriver\\chromedriver.exe");
		// Create a new instance of the Firefox driver
		 driver = new ChromeDriver();
		driver.navigate().to("https://www.tajawal.ae/en");
		driver.manage().window().maximize();
		
  }

  @AfterMethod
  public void afterMethod() 
  {
	  driver.quit();
  }

}
